
package QuestionFour;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        FrameProvider frameProv = new FrameProvider();
        
        Scanner sc = new Scanner(System.in);
        int rows, cols, fontSize;
        System.out.println("Enter font size:");
        fontSize = sc.nextInt();
        try{
            System.out.println("Insert number of rows:");
            rows = sc.nextInt();
            System.out.println("Insert number of columns");
            cols = sc.nextInt();
            MoreThan200RowsException.CheckRows(rows, cols);
            String[][] data = new String[rows][cols];
            String[] columnTitles = new String[cols];
            for(int i = 0; i < cols; i++){
                int item = i + 1;
                System.out.println("Enter heading for "+item+" column : ");
                columnTitles[i] = sc.next();
            }

            for(int i = 0; i < rows; i++){
                int item = i + 1;
                System.out.println("Enter data for "+item+" row: ");
                for(int j = 0; j < cols; j++){
                    System.out.println("Enter "+columnTitles[j]);
                    data[i][j] = sc.next();
                }
            }
        
        frameProv.setTableData(data, columnTitles, fontSize);
        frameProv.initFrame();
        }
        catch(MoreThan200RowsException ex){
            System.out.println(ex.getMessage());
        }
        
        
    }
}
