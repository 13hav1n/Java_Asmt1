
package QuestionFour;

import javax.swing.*;
public interface Featurable {
    void setTableBackground(JTable table);
    void setTableFont(JTable table, int fontSize);
    void setTableForground(JTable table);
}
