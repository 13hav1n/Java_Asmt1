package QuestionFour;

import java.awt.Color;
import java.awt.Font;
import java.util.Scanner;

public class ChoiceProvider {

    public Color colorChoice() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Select background color");
        System.out.println("1) Red");
        System.out.println("2) Green");
        System.out.println("3) Blue");
        System.out.println("4) Ofwhite");
        System.out.println("5) Pink");
        int choice = sc.nextInt();
        switch (choice) {
            case 1:
                return new Color(255, 0, 0);
            case 2:
                return new Color(0, 255, 0);
            case 3:
                return new Color(0, 0, 255);
            case 4:
                return new Color(0xdddddd);
            case 5:
                return new Color(191, 82, 188);
            default:
                System.out.println("Invalid background color selected!");
                return new Color(0, 0, 0);
        }
    }

    public Font fontFamilyChoice(int fontSize) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Select font style color");
        System.out.println("1) Cascadia code");
        System.out.println("2) Arial");
        System.out.println("3) Times new roman");
        System.out.println("4) monospace");
        System.out.println("5) consolas");
        int choice = sc.nextInt();
        switch (choice) {
            case 1:
                return new Font("Cascadia Mono",Font.PLAIN, fontSize);
            case 2:
                return new Font("Arial",Font.PLAIN, fontSize);
            case 3:
                return new Font("Times New Roman",Font.PLAIN, fontSize);
            case 4:
                return new Font("Monospace",Font.PLAIN, fontSize);
            case 5:
                return new Font("Consolas",Font.PLAIN, fontSize);
            default:
                System.out.println("Invalid background Font selected!");
                return new Font("Arial",Font.PLAIN, fontSize);
        }
    }
}
