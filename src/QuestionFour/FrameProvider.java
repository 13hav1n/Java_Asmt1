package QuestionFour;

import javax.swing.*;
import java.awt.*;

public class FrameProvider extends JFrame {
   JTable table;
   
    JavaDataTable dt = new JavaDataTable();
   public void setTableData(Object[][] data, Object[] columnTitles, int fontSize){
       table = new JTable(data, columnTitles);
       dt.setTableBackground(table);
       dt.setTableForground(table);
       dt.setTableFont(table, fontSize);
   }
   
   public void initFrame(){
       setTitle("Question 4");
       setLayout(new FlowLayout());
       setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       add(new JScrollPane(table));
       
       setVisible(true);
       pack();
   }
   
}
