package QuestionFour;

public class MoreThan200RowsException extends Exception{
    MoreThan200RowsException(String message){
        super(message);
    }
    public static void CheckRows(int RowCount, int ColCount) throws MoreThan200RowsException{
        if(RowCount > 200 || ColCount > 200){
            throw new MoreThan200RowsException("Rows or columns are exceeding predefined limit");
        }
    }
}

