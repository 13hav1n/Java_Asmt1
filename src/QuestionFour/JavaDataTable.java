package QuestionFour;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class JavaDataTable implements Featurable{
    String BackgroundColor;
    String FontSize;
    String FontFamily;
    String FontColor;
    ChoiceProvider choiceProv = new ChoiceProvider();
    @Override
    public void setTableBackground(JTable table) {
       table.setBackground(choiceProv.colorChoice());
    }

    @Override
    public void setTableFont(JTable table, int fontSize) {
       table.setFont(choiceProv.fontFamilyChoice(fontSize));
    }

    @Override
    public void setTableForground(JTable table) {
       table.setForeground(choiceProv.colorChoice());
    }
    
}
