package QuestionFive;

import java.sql.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

public class DBOperations {

    private PreparedStatement ps;
    Connection con = null;

    DBOperations() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/addressbookdb", "root", "");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Connection failure", JOptionPane.ERROR_MESSAGE);
        }
    }

    public int InsertRow(AddressBook objAb) {
        try {
            ps = con.prepareStatement("INSERT INTO tbladdressrecords(Name, HomeAddr, Phone, BusinessAddr, FaxMcnNo, PagerNo, MaritalStatus, Children, AnnualIncome, CellularNo) VALUES(?,?,?,?,?,?,?,?,?,?)");
            ps.setString(1, objAb.Name);
            ps.setString(2, objAb.HomeAddr);
            ps.setLong(3, objAb.PhoneNumber);
            ps.setString(4, objAb.BusinessAddr);
            ps.setString(5, objAb.FaxMachineNumber);
            ps.setString(6, objAb.Pager);
            ps.setString(7, objAb.MaritalStatus);
            ps.setInt(8, objAb.NumberOfChidren);
            ps.setDouble(9, objAb.AnnualIncome);
            ps.setLong(10, objAb.CellularPhoneNumber);
            int insertStatus = ps.executeUpdate();
            JOptionPane.showMessageDialog(null, insertStatus + "Rows inserted!", "Insertion success", JOptionPane.INFORMATION_MESSAGE);
            return insertStatus;

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Insertion failure", JOptionPane.ERROR_MESSAGE);
            return -1;
        }
    }
    public void DeleteRow(long PhoneNumber) {
        try {
            ps = con.prepareStatement("DELETE FROM tbladdressrecords where Phone=?");
            ps.setLong(1, PhoneNumber);
            int deleteStatus = ps.executeUpdate();
            JOptionPane.showMessageDialog(null, deleteStatus + "Rows deleted!", "Deletion success", JOptionPane.INFORMATION_MESSAGE);

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Deletion failure", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public ArrayList<AddressBook> GetTableData(){
        ArrayList<AddressBook> dataList = new ArrayList<>();
        try {
            Statement statement = con.createStatement();
            statement.executeQuery("SELECT * FROM tbladdressrecords");
            ResultSet rs = statement.getResultSet();
            while(rs.next()){
                String Name = rs.getString("Name");
                String HomeAddress = rs.getString("HomeAddr");
                long PhoneNumber = rs.getLong("Phone");
                String BusinessAddr = rs.getString("BusinessAddr");
                String FaxMachineNo = rs.getString("FaxMcnNo");
                String PagerNo = rs.getString("PagerNo");
                long CellularNo = rs.getLong("CellularNo");
                String MaritalStatus = rs.getString("MaritalStatus");
                int NoChildren = rs.getInt("Children");
                double AnnualIncome = rs.getDouble("AnnualIncome");
                dataList.add(new AddressBook(Name,HomeAddress,PhoneNumber,BusinessAddr,FaxMachineNo,PagerNo,CellularNo,MaritalStatus,NoChildren,AnnualIncome));
            }
        } catch (Exception ex) {
            
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Retrival failure", JOptionPane.ERROR_MESSAGE);
        }
        return dataList;
    }
    
    public int UpdateRow(long Phone, AddressBook objAb){
        try {
            ps = con.prepareStatement("UPDATE tbladdressrecords SET Name=?, HomeAddr=?, Phone=?, BusinessAddr=?, FaxMcnNo=?, PagerNo=?, MaritalStatus=?, Children=?, AnnualIncome=?, CellularNo=? WHERE Phone=?");
            ps.setString(1, objAb.Name);
            ps.setString(2, objAb.HomeAddr);
            ps.setLong(3, objAb.PhoneNumber);
            ps.setString(4, objAb.BusinessAddr);
            ps.setString(5, objAb.FaxMachineNumber);
            ps.setString(6, objAb.Pager);
            ps.setString(7, objAb.MaritalStatus);
            ps.setInt(8, objAb.NumberOfChidren);
            ps.setDouble(9, objAb.AnnualIncome);
            ps.setLong(10, objAb.CellularPhoneNumber);
            ps.setLong(11, Phone);
            int updateStatus = ps.executeUpdate();
            JOptionPane.showMessageDialog(null, updateStatus + "Rows updated!", "Updation success", JOptionPane.INFORMATION_MESSAGE);
            return updateStatus;

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Unable to update record" + ex.getMessage(), "Updation failure", JOptionPane.ERROR_MESSAGE);
            return -1;
        }
    }
}
