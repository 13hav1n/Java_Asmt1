package QuestionFive;


public class AddressBook {
    public String Name;
    public String HomeAddr;
    public long PhoneNumber;
    public String BusinessAddr;
    public String FaxMachineNumber;
    public String Pager;
    public long CellularPhoneNumber;
    public String MaritalStatus;
    public int NumberOfChidren;
    public double AnnualIncome;
    
    public AddressBook(){}
    public AddressBook(
        String Name, 
        String HomeAddr, 
        long PhoneNumber, 
        String BusinessAddr, 
        String FaxMachineNumber, 
        String Pager, 
        long CellularPhoneNumber,
        String MaritalStatus,
        int NumberOfChildren,
        double AnnualIncome
        ){
            this.Name = Name;
            this.HomeAddr = HomeAddr;
            this.PhoneNumber = PhoneNumber;
            this.BusinessAddr = BusinessAddr;
            this.CellularPhoneNumber = CellularPhoneNumber;
            this.Pager = Pager;
            this.FaxMachineNumber = FaxMachineNumber;
            this.MaritalStatus = MaritalStatus;
            this.NumberOfChidren = NumberOfChildren;
            this.AnnualIncome = AnnualIncome;
    }
    
}
