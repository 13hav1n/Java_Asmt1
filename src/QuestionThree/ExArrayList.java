package QuestionThree;

import java.util.*;
import java.util.Scanner;
import java.util.Iterator;
import java.util.ArrayList;

public class ExArrayList {

    public static void CollectionSelectMenu(boolean isGeneric, int size) {
        if (isGeneric) {
            ArrayList<Integer> arrList = new ArrayList<Integer>();
            PopulateList(arrList, null, size);
        } else {
            ArrayList arrList = new ArrayList();
            PopulateList(null, arrList, size);
        }
    }

    public static void PopulateList(ArrayList<Integer> genArLst, ArrayList arLst, int size) {
        Scanner sc = new Scanner(System.in);
        try {

            if (genArLst == null) {
                for (int i = 0; i < size; i++) {
                    System.out.printf("Enter element %d (Non genric): ", i + 1);
                    arLst.add(sc.next());
                }
                DisplayList(null, arLst);
            }
            if (arLst == null) {
                for (int i = 0; i < size; i++) {
                    System.out.printf("Enter integer only element %d (Generic): ", i + 1);
                    genArLst.add(sc.nextInt());
                }
                DisplayList(genArLst, null);
            }
        } catch (InputMismatchException ex) {
            System.out.println("Invalid input type: " + ex.getMessage());
        }
    }

    public static void DisplayList(ArrayList<Integer> genArLst, ArrayList arLst) {
        if (genArLst == null) {
            for (var s : arLst) {
                System.out.println("Element of non generic list : " + s);
            }
        }
        if (arLst == null) {
            for (Integer s : genArLst) {
                System.out.println("Element generic list : " + s);
            }
        }
    }

    public static void main(String[] args) {
        int genericChoice;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter size of the arra list : ");
        int size = sc.nextInt();
        System.out.println("Select type generic or non generic");
        do {
            System.out.println("1) With generic");
            System.out.println("2) Without generic");
            System.out.println("3) Exit");
            genericChoice = sc.nextInt();
            if (genericChoice == 3) {
                System.exit(0);
            }
            switch (genericChoice) {
                case 1:
                    CollectionSelectMenu(true, size);
                    break;
                case 2:
                    CollectionSelectMenu(false, size);
                    break;
                case 3:
                    break;
                default:
                    System.out.println("Invalid choice!");
            }
        } while (genericChoice < 3);

    }
}
