package QuestionThree;

import java.util.*;

public class ExHashMap {
    public static void main(String[] args){
        HashMap<String, Integer> hMap = new HashMap<String, Integer>();
        
        // Product and price as the item of has map
        hMap.put("Tshirt", 650);
        hMap.put("Cap", 160);
        hMap.put("Jeans", 890);
        hMap.put("Shoes", 1869);
        System.out.println("Hash map :"+ hMap);
        
        System.out.println("Price of the jeans is "+ hMap.get("Jeans") + "Rs");
        
        System.out.println("Keys : " + hMap.keySet());
        System.out.println("Values : " + hMap.values());
        System.out.println("Key/Values : " + hMap.entrySet());
        
        hMap.replace("Cap", 200);
        System.out.println("Price of the Cap is : " + hMap.get("Cap") + " Rs");
        
        hMap.remove("Shoes"); // remove Element which contains "Shoes" as key
        
    }
}
