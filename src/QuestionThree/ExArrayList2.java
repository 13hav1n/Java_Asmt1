package QuestionThree;

import java.util.*;

public class ExArrayList2 {
    public static void main(String[] args){
        // Non generic array list 
        ArrayList arrList = new ArrayList();
        arrList.add("Banana");
        arrList.add("Grapes");
        arrList.add(12);
        arrList.add(12.3);
        /*
        note:  In non generic array list we can add any type of data
        */
        for(var item : arrList){
            System.out.println("Item of non generic array list: " + item);
           
        }
        
        // Generic Array List
        ArrayList<String> genArrLst = new ArrayList<String>();
        genArrLst.add("Banana");
        genArrLst.add("Grapes");
        genArrLst.add("Pineapple");
        genArrLst.add("Kiwi");
        genArrLst.add("Apple");
        // genArrLst.add(23); // gives a type missmatch exception
        Iterator it = genArrLst.iterator();
        while(it.hasNext()){
            System.out.println("Item of arraylist : "+ it.next());
        }
        genArrLst.remove("Apple");
        
        // Assining new iterator instance beacuse of the change in array list to avoid 'Concurrent Modification Excpetion'
        it = genArrLst.iterator();
        while(it.hasNext()){
            System.out.println("After removing item : "+ it.next());
        }
        
        // Use of indexOf method
        int index = genArrLst.indexOf("Grapes");
        System.out.println("Index of value Grapes is " + index);
        
        
        // Removing all the element from the generic array list
        // genArrLst.removeAll(genArrLst);
        
        System.out.println("Size of the array list is : " + genArrLst.size());
        
        // Sorting generic list 
         genArrLst.sort(Comparator.naturalOrder());
        // genArrLst.sort(Comparator.reverseOrder());
        for(String s : genArrLst){
            System.out.println("Soretd list element : " + s);
        }
        
    }
}
