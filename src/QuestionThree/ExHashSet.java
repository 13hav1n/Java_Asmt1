package QuestionThree;

import java.util.*;
import java.util.Iterator;
import java.util.LinkedList;

public class ExHashSet {

    public static void main(String[] args) {
        HashSet<String> hs = new HashSet<>();
        hs.add("Apple");
        hs.add("Pineapple");
        hs.add("Strawberry");
        hs.add("Orange");
        hs.add("Kewi");

        // Removing an element
        hs.remove("Pineapple");

        //Removinig ahs the elements
        //hs.removeAhs(hs);
        Iterator itr = hs.iterator();
        while (itr.hasNext()) {
            System.out.println("element of hash set: " + itr.next());
        }
        System.out.println("Size of the hash set is : " + hs.size());
        
        HashSet<Integer> n1 = new HashSet<>();
        n1.add(1);
        n1.add(2);
        n1.add(5);
        
        HashSet<Integer> n2 = new HashSet<>();
        n2.add(2);
        n2.add(5);
        n2.add(6);
        
        n1.retainAll(n2);
        for(int i : n1){
            System.out.println("Intersection : " + i);
        }
        
    }
}
