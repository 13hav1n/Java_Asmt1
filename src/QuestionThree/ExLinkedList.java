package QuestionThree;
import java.util.*;

public class ExLinkedList {
    public static void main(String[] args){
        LinkedList<String> ll = new LinkedList<>();
        ll.add("Apple");
        ll.add("Pineapple");
        ll.add("Strawberry");
        ll.add("Orange");
        ll.add("Kewi");
        
        // Removing an element
        ll.remove("Pineapple");
        
        //Rmovinig all the elements
        //ll.removeAll(ll);
        
        System.out.println("Element at index 1 is : " + ll.get(1));
        
        ll.set(1, "Blueberry");
        System.out.println("Element at index 1 is : " + ll.get(1));
        
        ll.addFirst("Guvava");
        
        Iterator itr = ll.iterator();
        while(itr.hasNext()){
            System.out.println("element of linked list : " + itr.next());
        }
        System.out.println("Size of the linked list is : " + ll.size());

    }
}
