package QuestionThree;

import java.util.*;

public class ExTreeMap {

    public static void main(String[] args) {
        TreeMap<Integer, String> tMap = new TreeMap<>();
        TreeMap<Integer, String> tMapTemp = new TreeMap<>();
        tMap.put(1, "Python");
        tMap.put(2, "Javascript");
        tMap.put(3, "Java");
        tMap.put(4, "Ruby");
        tMap.put(5, "C++");

        tMapTemp.put(6, "C");
        tMapTemp.put(7, "Go");
        tMapTemp.put(8, "Ada");

        tMap.putAll(tMapTemp);

        System.out.println("Tree map is : " + tMap);

        System.out.println("Langauge at 4 is " + tMap.get(4));

        System.out.println("Keys : " + tMap.keySet());
        System.out.println("Values : " + tMap.values());
        System.out.println("Key/Values : " + tMap.entrySet());

        tMap.replace(5, "Rust");
        System.out.println("Langauge at 2 is : " + tMap.get(2));

        tMap.remove(8);
    }
}
