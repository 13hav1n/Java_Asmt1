package QuestionThree;

import java.util.*;
import java.util.stream.*;

class Employee {

    public int Id;
    public String Name;
    public String Desig;
    public double Salary;
    public String City;

    Employee() {
    }

    Employee(int Id, String Name, String Desig, double Salary, String City) {
        this.Id = Id;
        this.Name = Name;
        this.Desig = Desig;
        this.Salary = Salary;
        this.City = City;
    }
}

public class ExStreams {

    public static void main(String[] args) {
        ArrayList<Employee> lstEmployees = new ArrayList<>();
        lstEmployees.add(new Employee(1, "Bhavin", "Product Manager", 70000, "Surat"));
        lstEmployees.add(new Employee(2, "Kunal", "Programmer", 30000, "Valsad"));
        lstEmployees.add(new Employee(3, "Kiyan", "Programmer", 30000, "Banglor"));
        lstEmployees.add(new Employee(4, "Alex", "TL", 50000, "Kolkata"));
        lstEmployees.add(new Employee(5, "John", "Sales", 22000, "Surat"));

        // Retriving name of employees
        List<String> empNameListMap = lstEmployees.stream().map(item -> item.Name).collect(Collectors.toList());
        for (String item : empNameListMap) {
            System.out.println(item);
        }

        // Filter 
        List<Employee> empListFilter = lstEmployees.stream().filter(x -> x.Salary >= 50000).collect(Collectors.toList());
        System.out.println("-> Name of Employees with the highest salary");
        for (Employee item : empListFilter) {
            System.out.println(item.Name);
        }

        //sorted
        List<String> empListSort = empNameListMap.stream().sorted().collect(Collectors.toList());
        System.out.println("-> Sorted list");
        for (String item : empListSort) {
            System.out.println(item);
        }

        // forEach
        lstEmployees.stream().map(x -> x.Salary / 2).forEach(elem -> System.out.println(elem));

        // reduce
        List<Integer> nums = Arrays.asList(2, 4, 5, 6, 7, 7, 3, 2, 6, 10);
        int EvenSum = nums.stream().filter(x -> x % 2 == 0).reduce(0, (res, item) -> {
            System.out.println("Reduce result :" + res);
            System.out.println("Reduce Item :" + item);
            return res + item;
        });
        System.out.println(EvenSum);
    }
}
