
package QuestionThree;

import java.util.*;

public class ExTreeSet {
    public static void main(String[] args){
        TreeSet<String> ts = new TreeSet<>();
        ts.add("Apple");
        ts.add("Pineapple");
        ts.add("Strawberry");
        ts.add("Orange");
        ts.add("Kewi");

        // Removing an element
        ts.remove("Pineapple");

        //Removinig at the elements
        //ts.removeAt(ts);
        Iterator itr = ts.iterator();
        while (itr.hasNext()) {
            System.out.println("element of Tree set list : " + itr.next());
        }
        System.out.println("Size of the tree set is : " + ts.size());
        
        TreeSet<Integer> n1 = new TreeSet<>();
        n1.add(1);
        n1.add(2);
        n1.add(5);
        
        TreeSet<Integer> n2 = new TreeSet<>();
        n2.add(2);
        n2.add(5);
        n2.add(6);
        
        n1.retainAll(n2);
        for(int i : n1){
            System.out.println("Intersection : " + i);
        }
    }
}
