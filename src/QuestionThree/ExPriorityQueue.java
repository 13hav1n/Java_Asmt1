package QuestionThree;
import java.util.*;


public class ExPriorityQueue {
    public static void Iterate(PriorityQueue<Integer> q){
        Iterator it = q.iterator();
        while(it.hasNext()){
            System.out.println("Element of the queue is : " + it.next());
        }
    }
    public static void main(String[] args){
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        pq.add(3);
        pq.add(1);
        pq.add(8);
        pq.add(2);
        pq.add(6);
        Iterator it = pq.iterator();
        Iterate(pq);
        
        // offer - insert the specify element in the queue if the queue is full, false would be return
        pq.offer(90);
        System.out.println("---After using offer method---");
        Iterate(pq);
        
        // peek return the head of the priority queue
        System.out.println("Peeking the element of the queue : " + pq.peek());

        // pq.remove(90);
        
        //pq.removeAll(pq); // removes all the element from the collection
        
        //poll - returns and remove the head of the queue
        System.out.println("Polled element:"+pq.poll());
        System.out.println("After using poll method:");
        Iterate(pq);
        
        System.out.println("Size of the priority queue is :" + pq.size());
    }
}
