/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package QuestionOne.pkg;

/**
 *
 * @author Lenovo
 */
public abstract class Shape {
    private String color;
    abstract public double getArea();
    abstract public String toString();
}
