/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package QuestionOne.pkg;

/**
 *
 * @author Lenovo
 */
public class Rectangle extends Shape {
    private int length;
    private int width;
    public Rectangle(int length, int width){
        this.length = length;
        this.width = width;
    }
    public double getArea(){
        return length * width;
    }
    public String toString(){
        return "Area of the Rectangle is : " + getArea();
    }
}
