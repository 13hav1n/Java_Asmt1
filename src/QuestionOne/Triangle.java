/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package QuestionOne.pkg;

/**
 *
 * @author Lenovo
 */
public class Triangle extends Shape {
    private int base;
    private int height;
    public Triangle(int base, int height){
        this.base = base;
        this.height = height;
    }
    public double getArea(){
        return 0.5 * base * height;
    }
    public String toString(){
        return "Area of the Triangle is : " + getArea();
    }
}
