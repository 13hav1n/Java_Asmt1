package QuestionOne.pkg;
import java.util.*;

/*
Write a Java Program for the following UML class diagram Show the use of Inheritence, Polymorphism, Upcasting and Downcasting features of Java
*/
public class Question1 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter height of the Reactangle: ");
        int length = sc.nextInt();
        System.out.println("Enter width of the Reactangle: ");
        int width = sc.nextInt();
        System.out.println("Enter base of the Triangle: ");
        int base = sc.nextInt();
        System.out.println("Enter height of the Triangle: ");
        int height = sc.nextInt();
        
        // Example of Upcasting
        Shape instShape1 = new Rectangle(length, width);
        Shape instShape2 = new Triangle(base, height);
        System.out.println(instShape1.toString());
        System.out.println(instShape2.toString());
        
        // Example of Downcasting
        Rectangle rect = (Rectangle)instShape1;
        Triangle tri = (Triangle)instShape2;
        System.out.println("Area of Reactangle retrived using downcasting: " + rect.getArea());
        System.out.println("Area of Triangle retrived using downcasting: " +tri.getArea());
        
        
    }    
}
